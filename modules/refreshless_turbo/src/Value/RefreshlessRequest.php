<?php

declare(strict_types=1);

namespace Drupal\refreshless_turbo\Value;

use Symfony\Component\HttpFoundation\Request;

/**
 * Value object wrapping a potential RefreshLess Turbo request.
 */
class RefreshlessRequest {

  /**
   * Name of the header indicating a RefreshLess Turbo request.
   */
  protected const HEADER_NAME = 'x-refreshless-turbo';

  /**
   * Constructor; saves dependencies.
   */
  public function __construct(
    protected readonly Request $request,
  ) {}

  /**
   * Whether this is a Turbo request.
   *
   * @return boolean
   */
  public function isTurbo(): bool {
    return $this->request->headers->has(self::HEADER_NAME);
  }

  /**
   * Get the HTTP header name identifying a RefreshLess request.
   *
   * @return string
   */
  public static function getHeaderName(): string {
    return self::HEADER_NAME;
  }

}
