<?php

declare(strict_types=1);

namespace Drupal\refreshless\Value;

use Symfony\Component\HttpFoundation\Request;

/**
 * Interface for a RefreshLess page state value object.
 */
interface PageStateInterface {

  /**
   * Get any libraries already on the page.
   *
   * @return string[]
   */
  public function getLibraries(): array;

  /**
   * Set the libraries already on the page.
   *
   * @param string[]|string $libraries
   *
   * @return self
   *   The instance for chaining.
   */
  public function setLibraries(array|string $libraries): self;

  /**
   * Get the theme token, if any.
   *
   * @return string
   */
  public function getThemeToken(): string;

  /**
   * Set the theme token.
   *
   * @param string $themeToken
   *
   * @return self
   *   The instance for chaining.
   */
  public function setThemeToken(string $themeToken): self;

  /**
   * Get the theme name.
   *
   * @return string
   *   The theme name.
   */
  public function getTheme(): string;

  /**
   * Set the theme name.
   *
   * @param string $theme
   *   The theme name to set.
   *
   * @return self
   *   The instance for chaining.
   */
  public function setTheme(string $theme): self;

  /**
   * Format the page state into an array suitable for drupalSettings.
   *
   * @return array
   *   An array of page state information containing 'libraries', 'theme', and
   *   'themeToken' keys.
   */
  public function toDrupalSettings(): array;

  /**
   * Create from a provided drupalSettings array.
   *
   * @param array $settings
   *   A array containing 'libraries', 'theme', and 'themeToken' keys.
   *
   * @return self
   *   A new instance with values set.
   */
  public static function fromDrupalSettings(array $settings): self;

  /**
   * Create from a provided cookie value.
   *
   * @param string $value
   *   The cookie value to create from.
   *
   * @return self
   *   A new instance with values set.
   */
  public static function fromCookieValue(string $value): self;

  /**
   * Create from a provided Symfony Request object.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object to create from.
   *
   * @return self
   *   A new instance with values set.
   */
  public static function fromRequest(Request $request): self;

  /**
   * Get the page state cookie name.
   *
   * @return string
   *   The page state cookie name.
   */
  public static function getCookieName(): string;

}
