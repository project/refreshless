<?php

declare(strict_types=1);

namespace Drupal\refreshless\Service;

use Drupal\refreshless\Value\PageStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Interface for a RefreshLess page state factory service.
 */
interface PageStateFactoryInterface {

  /**
   * Get the page state value object class.
   *
   * @return string
   *   A fully qualified class name.
   */
  public function getClass(): string;

  /**
   * Create a page state value object from a provided drupalSettings array.
   *
   * @param array $settings
   *   A array containing 'libraries', 'theme', and 'themeToken' keys.
   *
   * @return \Drupal\refreshless\Value\PageStateInterface
   *   A new a page state value object instance with values set.
   */
  public function fromDrupalSettings(array $settings): PageStateInterface;

  /**
   * Create a page state value object from a provided cookie value.
   *
   * @param string $value
   *   The cookie value to create from.
   *
   * @return \Drupal\refreshless\Value\PageStateInterface
   *   A new a page state value object instance with values set.
   */
  public function fromCookieValue(string $raw): PageStateInterface;

  /**
   * Create a page state value object from a provided Symfony Request object.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object to create from.
   *
   * @return \Drupal\refreshless\Value\PageStateInterface
   *   A new a page state value object instance with values set.
   */
  public function fromRequest(Request $request): PageStateInterface;

}
