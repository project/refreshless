<?php

declare(strict_types=1);

namespace Drupal\refreshless\Service;

use Drupal\refreshless\Service\PageStateFactoryInterface;
use Drupal\refreshless\Value\PageState;
use Drupal\refreshless\Value\PageStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Default implementation of the RefreshLess page state factory service.
 */
class PageStateFactory implements PageStateFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function getClass(): string {
    return PageState::class;
  }

  /**
   * {@inheritdoc}
   */
  public function fromDrupalSettings(array $settings): PageStateInterface {
    return $this->getClass()::fromDrupalSettings($settings);
  }

  /**
   * {@inheritdoc}
   */
  public function fromCookieValue(string $raw): PageStateInterface {
    return $this->getClass()::fromCookieValue($raw);
  }

  /**
   * {@inheritdoc}
   */
  public function fromRequest(Request $request): PageStateInterface {
    return $this->getClass()::fromRequest($request);
  }

}
