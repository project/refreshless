* [Drupal core](https://www.drupal.org/project/drupal) 10.3 or newer

* [PHP](https://www.php.net/) 8.1 or newer

* [Composer](https://getcomposer.org/)

* [`cweagans/composer-patches` 1.x](https://github.com/cweagans/composer-patches/tree/1.x)
