![Stylized "RefreshLess"](assets/optimized/title.svg)

This is the documentation for [the RefreshLess module](https://www.drupal.org/project/refreshless).

# [User guide](user/index.md)

This is the documentation for [installing](user/install.md) and using RefreshLess on a web site/web app.

# [Integrating](integrating/index.md)

This is the documentation for integrating with or extending the RefreshLess module. You can find [guidelines for writing your behaviours](integrating/behaviours.md), a [reference of all events](integrating/events.md) that we trigger, and more.

# [Developing](developing/index.md)

This is the documentation for getting involved with the development of RefreshLess itself. See [where you can get started helping out](developing/index.md).

# [About](about/index.md)

Reach out to us in [our chat or issue queue](about/contact.md) if you need help or found a bug. Are you interested in learning about the arguments for server-rendered web sites/web apps? [Read the motivation page](about/motivation.md).
