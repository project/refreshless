# Integrating

This is the documentation for integrating with or extending the RefreshLess
module. Generally speaking, RefreshLess is intended to be installed and just
work without any further configuration required. However, if you want to build
more advanced interactions based on what RefreshLess does or ensure your
front-end code works well with this module, you're in the right place.

[Behaviours](behaviours.md)
:   Guidelines for writing your JavaScript behaviours.

[Events](events.md)
:   Reference for all the events that we trigger and the information they provide.

[API](api.md)
:   Our minimal API.
