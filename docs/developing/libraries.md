We ship with Turbo and other libraries bundled with this project to make
installation as easy as possible for end users. However, updating and patching
these requires additional set up using [Yarn 3 or
newer](https://yarnpkg.com/getting-started/install) and some basic knowledge of
the commandline.

## Requirements

In addition to the [main requirements for this
project](../user/requirements.md), the following are required to update
front-end dependencies and patch Turbo:

* A front-end package manager that supports both [the Plug'n'Play install strategy](https://yarnpkg.com/features/pnp) and [workspaces](https://yarnpkg.com/features/workspaces):

  * [Yarn 3 or newer](https://yarnpkg.com/getting-started/install) (Yarn 1 *will not work*; Yarn 2 *might work* but has not been tested)

  * [pnpm](https://pnpm.io/) may also work but has not been tested

## Installation

The instructions here are for [Yarn](https://yarnpkg.com/), but can be adapted
for [pnpm](https://pnpm.io/) or other package managers.

The following assumes you're using [Yarn
workspaces](https://yarnpkg.com/features/workspaces) from the root of your
Drupal installation, i.e. where your root `composer.json` is located, one level
above the web directory. You must also be using [the Plug'n'Play install
strategy](https://yarnpkg.com/features/pnp), which is the default in modern
Yarn. You'll need to have a `package.json` in the root directory containing a
`workspaces` array:

```json
{
  "workspaces": [
    "web/modules/contrib/refreshless/**!(vendor)/*"
  ]
}
```

If you already have other workspace paths defined, just merge that in.

Then, run `yarn add drupal-refreshless-turbo@workspace:^2` and Turbo will be
downloaded and extracted to the correct location.

### Patching

The following describes how to apply our Turbo patches [via
Yarn](https://yarnpkg.com/features/patching) but can also be [adapted for
pnpm](https://pnpm.io/cli/patch). We may eventually contribute to Turbo and
remove the need for patches, but until that time, patching will be required to fix various issues. *Note that this has only been tested on Linux, may work on
macOS, and will likely fail on Windows.* The following scripts are provided:

- `yarn workspace drupal-refreshless-turbo vendorize` - This will extract several libraries to the module folder, including an unaltered (unpatched) copy of Turbo. This is accomplished via the [`@consensus.enterprises/pnp-vendorize`](https://www.npmjs.com/package/@consensus.enterprises/pnp-vendorize) package.

- `yarn workspace drupal-refreshless-turbo patch-turbo` - This will apply all of our patches to Turbo. Note that this will fail if Turbo is already patched.

- `yarn workspace drupal-refreshless-turbo unpatch-turbo` - This will unpatch a patched Turbo, reversing the patches.

- `yarn workspace drupal-refreshless-turbo patch-debug-order` - Prints the order that patches would be applied to Turbo; note that this doesn't actually patch and is for debugging only.
