We use Drupal's functional JavaScript PHPUnit framework for our tests. Please see [the local development set up](local.md#testing) if you don't already have the capability to run those. When using DDEV, you can run them via:

```shell
ddev exec phpunit --group=refreshless
```

Adapt as needed if not using DDEV.
