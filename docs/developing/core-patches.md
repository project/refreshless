Because this project needs to alter some low level functionality in Drupal core,
we've run into limitations or deficiencies where it's not feasible to decorate
a service or replace something with our own code; in these cases we patch core
to fix something or add support for something else. Ideally, these would be merged into core one day.

[JS aggregation should account for "async" and "defer" attributes [#1587536]](https://www.drupal.org/project/drupal/issues/1587536)
:   This is needed so that we can force all JavaScript into the `<head>` but add the `defer` attribute as [recommended by the Turbo documentation](https://turbo.hotwired.dev/handbook/building#loading-your-application%E2%80%99s-javascript-bundle) to avoid having Turbo re-evaluate JavaScript it finds in the `<body>`.

[Allow setting aggregation groups for js files in library definitions [#3232810]](https://www.drupal.org/project/drupal/issues/3232810)
:   This allows us to enable aggregation for our own module's JavaScript without the possibility it'll be bundled into another aggregate and potentially evaluated more than once. We previously wrapped all of our JavaScript in `once()` callbacks, but this allows for cleaner code and lower level control.

[New navigation does not displace if detached and attached again [#3487905]](https://www.drupal.org/project/drupal/issues/3487905)
:   The new experimental Navigation module needs some extra help because its JavaScript doesn't seem to have been rewritten with a persistent process in mind.
