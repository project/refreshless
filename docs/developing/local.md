We generally recommend [DDEV](https://ddev.com/) because it provides most or all of the above for you out of the box, and [helps you get a Drupal project up and running](https://www.drupal.org/docs/develop/local-server-setup/docker-based-development-environments-for-macos-linux-and-windows) much faster while also isolating it from the rest of your operating system via containerization.

## Testing

Additionally, we make heavy use of PHPUnit functional JavaScript tests, and the easiest way to get that set up is via the [DDEV Selenium Standalone Chrome add-on](https://github.com/ddev/ddev-selenium-standalone-chrome).
