# Developing

This contains the documentation for developing RefreshLess. If you'd like to
help out but aren't sure where to start, reach out to us via [our chat or issue
queue](../about/contact.md).

[Local development](local.md)
:   Information on setting up your local development environment.

[Drupal core patches](core-patches.md)
:   This lists the various Drupal core patches we provide and the reasons for them.

[Libraries (Turbo, etc.)](libraries.md)
:   This contains instructions for updating the various front-end libraries we ship with and for patching Turbo.

[Testing](testing.md)
:   This contains information on automated testing.
