# About

[Motivation](motivation.md)
:   What are the arguments for server-rendered web sites/web apps, and why do they matter?

[Contact](contact.md)
:   Get in touch with us via our chat or issue queue.
